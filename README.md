# Metadata hook for monorepo development

## Background

This repo demonstrates using hatch for development of multiple related
projects in a monorepo.  This is simulated using the `flufl` namespace package
and several empty subpackages which have out-of-band releases already existing
on PyPI.


At the top level, you `hatch.toml` and `ruff.toml` scripts that operate across
the entire monorepo.  Inside the `libs/` directory, you have the `flufl`
namespace package subdirectory, plus `flufl.enum` and `flufl.lock` subpackage
subdirectories.  `flufl` depends on both subpackages, with the intent of
telling users

> You can install everything by doing `pip install flufl`

Note that the `flufl` package also has an optional dependency on `flufl.i18n`
such that we could tell users

> You can optionally also run `pip install flufl[i18n]`

# Goals

We want to accomplish several things with this monorepo layout

* Allow someone at the top level to run comprehensive tests, linting,
  etc.  These will be invoked by workflow automation, Jenkins, etc.
* Most developers will only work in a subpackage, so they should be able to
  run tests, linting, etc. against just the subpackage, submit PRs, and
  generally not worry about other subpackages, unless some regression occurs.
  
# Monorepo-wide commands

At the top level you should be able to run these commands against a matrix
which includes each of the subpackage subdirectories.

* `hatch run check` -- runs static analysis against all subpackages
* `hatch run test` -- runs the tests for all subpackages in all supported
  versions of Python
* `hatch run build` -- builds the sdists and wheels for all subpackages.
  Everything ends up in the top level `dist/` directory, which could then be
  uploaded with `hatch publish`.

# Subpackage commands

Within a single subpackage, you can run tests and precommit checks
(e.g. linting) only within that subpackage.  This is a great way to smoketest
your changes locally before you create a PR.

Let's say you're inside `libs/flufl.enum`.  You can run these commands.

* `hatch run precommit:check` -- basically, `hatch run check` for the subpackage.
* `hatch run test:check` -- just run the tests for this subpackage

# Dependencies

We have a dependency policy where packages _within_ the namespace have exact
pins on intra-package versions (e.g. `flufl` depends on `flufl.enum==6.1.0`).
Although not captured here, external dependencies float (i.e. they may specify
a floor version, and _possibly_ a major version ceiling, but generally not
exact pins).  This seems to be the best compromise so that we can ensure all
of _our_ packages work correctly together (and we own that stack), while
allowing consumers more latitude in which third party dependency versions they
install.

When doing local development, we want the `HEAD` of the monorepo to be
consistent, which means all tests, static analysis, and dependency checks
should be against the current state of the repo, _not_ against any released
packages.  However, because we obviously want released package metadata to
specify the correct dependencies when uploaded, we have to use "real"
dependency specifications, not `<package> @ file:` URIs.

The problem is that we need these latter `file` specifications when
"installing" packages within the monorepo, otherwise we can't ensure
intra-repo consistency.  This requires the use of a `hatch` metadata hook.
You can find this hook implemented in `scripts/hatch_hooks.py`, which rewrites
publicly consumable dependency specifications into development dependency
specifications.  This works well in most cases.

# The problem

This arrangement breaks in one specific but important case: creating and
testing the release branch.

In this case, we're bumping all the package version numbers, and we need the
PR for that (which might include change log entries, etc.) to pass CI.
Normally, I'd expect the same metadata hook to rewrite the intra-package
dependency specifications, but it appears that the hook isn't called when the
development environments are set up in a pristine clone (as would be the case
in CI).

This breaks because those intra-package dependency versions specify
_unreleased_ versions, so there's no way they can be resolved against PyPI,
because *the uploads haven't happened yet*!

# Workaround

Because we limit those intra-package dependencies to the namespace package, in
`libs/flufl/pyproject.toml` we can set `skip-install = true` to work around
this problem.  This isn't an ideal solution though because we actually _do_
want the subpackages to be installed, especially because in the future, we
expect other intra-package dependencies (e.g. `flufl.future` might eventually
depend on `flufl.enum` and we want to ensure that the `HEAD` always works).

So we need to get rid of the `skip-install=true` and still ensure that the
metadata hook gets run at the right times.  Is it a bug that we can't do this,
or expected behavior?

# Reproducing

The `main` branch works, because it uses the `skip-install`s.  The `broken`
branch is set up to demonstrate the breakage.  It removes the `skip-install`s
and sets the package metadata to use (absurdly high) unreleased version
numbers.  The idea being that those are versions we're about to release and we
need CI to test it.

**Possibly important note:** For consistency across platforms, our CI runs
this command, and we recommend it to our developers:

```shell
$ hatch config set dirs.env.virtual .hatch
```

We do this because normally `hatch` puts its caches and such in a
platform-specific location, which aside from being different on Linux, macOS,
and Windows, is also pretty inconvenient for macOS in particular (a common
development platform).  Making this consistent also helps CI debugging.

Run these commands in a *clean* (i.e. `git clean -dxffi`) `main` branch to see everything working:

```shell
$ hatch run check
$ hatch run test
$ hatch run build
```

Now, to see this fail, switch to the `broken` branch, run `git clean -dxffi`
and try those same commands.

The error I get is something like

```
$ hatch run check
──────────────────────────────────────────────────────── flufl ─────────────────────────────────────────────────────────
ERROR: Could not find a version that satisfies the requirement flufl-enum==24.0.0 (from flufl) (from versions: 3.0, 3.0.1, 3.1, 3.2, 3.3, 3.3.1, 3.3.2, 4.0, 4.0.1, 4.1, 4.1.1, 5.0, 5.0.1, 6.0, 6.0.1, 6.0.2, 6.1.0)
ERROR: No matching distribution found for flufl-enum==24.0.0
```

This only makes sense if the metadata hook doesn't get run, because obviously
`flufl.enum` version 24.0.0 isn't released yet; that's what this hypothetical
branch is all about!

