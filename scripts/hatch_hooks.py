import os
import sys
from pprint import pprint

from hatchling.metadata.plugin.interface import MetadataHookInterface


IN_MONOREPO = os.getenv('IN_MONOREPO')


class CustomMetadataHook(MetadataHookInterface):
    PLUGIN_NAME = 'ignored'

    def update(self, metadata: dict) -> None:
        print(f'{self.root=}', file=sys.stderr)
        if not IN_MONOREPO:
            print('SKIP: hatch metadata hook dependency rewrites', file=sys.stderr)
            return
        new_dependencies = []
        for dependency in metadata['dependencies']:
            if dependency.startswith('flufl'):
                package_name = dependency.split()[0]
                new_dependencies.append(f'{package_name} @ {{root:uri}}/../{package_name}')
            else:
                new_dependencies.append(dependency)
        metadata['dependencies'] = new_dependencies
        print('Monorepo dependencies:', file=sys.stderr)
        pprint(new_dependencies, stream=sys.stderr)
